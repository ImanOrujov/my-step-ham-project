$('.slick-carousel-main').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slick-carousel-slider',
    dots:false,
    arrows:false,
});

$('.slick-carousel-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slick-carousel-main',
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    prevArrow: $('.slick-carousel-arrow'),
    nextArrow: $('.slick-carousel-arrow-right')
});




let navbar = document.querySelector('.services-navbar');
let navbarItem = document.querySelectorAll('.services-navbar-h6');
let contentItem = document.querySelectorAll('.services-content-item');
navbarItem[0].classList.add('active');
navbarItem[0].classList.add('pseudo');
navbar.addEventListener('click',(event)=> {
  navbarItem.forEach((item)=>{
      item.classList.remove('active');
      item.classList.remove('pseudo');
  });
    event.target.classList.add('active');
    event.target.classList.add('pseudo');
    }
);

for (let i = 0; i<navbarItem.length;i++){                            //goes through every element to asign index to it and mash navbar and items together
    navbarItem[i].addEventListener('click',()=>{
        contentItem.forEach((item)=>{
            item.classList.add('invisible');                         //makes every item invisible
        });
        contentItem[i].classList.remove('invisible');         //makes visible only the item that we need
    })
}



let workNavbar = document.querySelector('.work-navbar');
let workContentItem = document.querySelectorAll('.work-table');
let workNavbarItem = document.querySelectorAll('.work-navbar-h6');
let workItem = document.querySelector('.work-item');
workNavbarItem[0].classList.add('work-navbar-h6-selected');
workNavbar.addEventListener('click', (event) => {
        workNavbarItem.forEach((item) => {
            item.classList.remove('work-navbar-h6-selected');
        });
        event.target.classList.add('work-navbar-h6-selected');
    }
);


let images = [
    {description: "graphic design", src: "img/graphic-design1.jpg"},
    {description: "graphic design", src: "img/graphic-design2.jpg"},
    {description: "graphic design", src: "img/graphic-design3.jpg"},
    {description: "graphic design", src: "img/graphic-design4.jpg"},
    {description: "graphic design", src: "img/graphic-design5.jpg"},
    {description: "graphic design", src: "img/graphic-design6.jpg"},
    {description: "graphic design", src: "img/graphic-design7.jpg"},
    {description: "graphic design", src: "img/graphic-design8.jpg"},
    {description: "graphic design", src: "img/graphic-design9.jpg"},
    {description: "graphic design", src: "img/graphic-design10.jpg"},
    {description: "graphic design", src: "img/graphic-design11.jpg"},
    {description: "graphic design", src: "img/graphic-design12.jpg"},
    {description: "web design", src: "img/web-design1.jpg"},
    {description: "web design", src: "img/web-design2.jpg"},
    {description: "web design", src: "img/web-design3.jpg"},
    {description: "web design", src: "img/web-design4.jpg"},
    {description: "web design", src: "img/web-design5.jpg"},
    {description: "web design", src: "img/web-design6.jpg"},
    {description: "web design", src: "img/web-design7.jpg"},
    {description: "wordpress", src: "img/wordpress1.jpg"},
    {description: "wordpress", src: "img/wordpress2.jpg"},
    {description: "wordpress", src: "img/wordpress3.jpg"},
    {description: "wordpress", src: "img/wordpress4.jpg"},
    {description: "wordpress", src: "img/wordpress5.jpg"},
    {description: "wordpress", src: "img/wordpress6.jpg"},
    {description: "wordpress", src: "img/wordpress7.jpg"},
    {description: "wordpress", src: "img/wordpress8.jpg"},
    {description: "wordpress", src: "img/wordpress9.jpg"},
    {description: "wordpress", src: "img/wordpress10.jpg"},
    {description: "landing page", src: "img/landing-page1.jpg"},
    {description: "landing page", src: "img/landing-page2.jpg"},
    {description: "landing page", src: "img/landing-page3.jpg"},
    {description: "landing page", src: "img/landing-page4.jpg"},
    {description: "landing page", src: "img/landing-page5.jpg"},
    {description: "landing page", src: "img/landing-page6.jpg"},
    {description: "landing page", src: "img/landing-page7.jpg"},

];


function all(images) {
    workItem.innerHTML='';
    images.forEach((item, index) => {
        if(index<12) {
            let imageCell = document.createElement('div');
            imageCell.classList.add("image-cell");
            workItem.appendChild(imageCell);
            let image = document.createElement('img');
            image.src = item.src;
            imageCell.appendChild(image);
            let notation = document.createElement('div');
            notation.classList.add('work-table-item-notation');
            notation.innerHTML = `<div><div class="work-table-item-images"><img class="pic pic1" src="img/workicon2.png"><img class="pic pic2" src="img/workicon1.png"> </div><div class="work-table-item-text"><h6 class="work-table-item-h6">creative design</h6><p class="work-table-item-p">${item.description}</p></div></div>`;

            imageCell.appendChild(notation);
            imageCell.addEventListener('mouseover', () => {
                notation.classList.add('visibility');
                notation.classList.remove('work-table-item-notation');

            });
            imageCell.addEventListener('mouseout', () => {
                notation.classList.remove('visibility');
                notation.classList.add('work-table-item-notation');

            })

        }});
    let loadingButton = document.createElement('button');
    loadingButton.classList.add('work-table-button');
    loadingButton.innerText="Load More";
    workItem.appendChild(loadingButton);
    loadingButton.addEventListener('click',()=>{
        loadingButton.parentElement.removeChild(loadingButton);
        images.forEach((item, index) => {
            if(index>=12 && index<24) {
                let imageCell = document.createElement('div');
                imageCell.classList.add("image-cell");
                workItem.appendChild(imageCell);
                let image = document.createElement('img');
                image.src = item.src;
                imageCell.appendChild(image);
                let notation = document.createElement('div');
                notation.classList.add('work-table-item-notation');
                notation.innerHTML = `<div><div class="work-table-item-images"><img class="pic pic1" src="img/workicon2.png"><img class="pic pic2" src="img/workicon1.png"> </div><div class="work-table-item-text"><h6 class="work-table-item-h6">creative design</h6><p class="work-table-item-p">${item.description}</p></div></div>`;

                imageCell.appendChild(notation);
                imageCell.addEventListener('mouseover', () => {
                    notation.classList.add('visibility');
                    notation.classList.remove('work-table-item-notation');

                });
                imageCell.addEventListener('mouseout', () => {
                    notation.classList.remove('visibility');
                    notation.classList.add('work-table-item-notation');

                })

            }});
        let secondLoadingButton = document.createElement('button');
        secondLoadingButton.innerText='Load More';
        secondLoadingButton.classList.add('work-table-button');
        workItem.appendChild(secondLoadingButton);
        secondLoadingButton.addEventListener('click',()=>{
            secondLoadingButton.parentElement.removeChild(secondLoadingButton);
            images.forEach((item, index) => {
                if(index>=24) {
                    let imageCell = document.createElement('div');
                    imageCell.classList.add("image-cell");
                    workItem.appendChild(imageCell);
                    let image = document.createElement('img');
                    image.src = item.src;
                    imageCell.appendChild(image);
                    let notation = document.createElement('div');
                    notation.classList.add('work-table-item-notation');
                    notation.innerHTML = `<div><div class="work-table-item-images"><img class="pic pic1" src="img/workicon2.png"><img class="pic pic2" src="img/workicon1.png"> </div><div class="work-table-item-text"><h6 class="work-table-item-h6">creative design</h6><p class="work-table-item-p">${item.description}</p></div></div>`;

                    imageCell.appendChild(notation);
                    imageCell.addEventListener('mouseover', () => {
                        notation.classList.add('visibility');
                        notation.classList.remove('work-table-item-notation');

                    });
                    imageCell.addEventListener('mouseout', () => {
                        notation.classList.remove('visibility');
                        notation.classList.add('work-table-item-notation');

                    })

                }});
        });


    });}

function graphicDesign(images) {
    workItem.innerHTML='';
    images.forEach(item => {
        if (item.description === "graphic design") {
            let imageCell = document.createElement('div');
            imageCell.classList.add("image-cell");
            workItem.appendChild(imageCell);
            let image = document.createElement('img');
            image.src = item.src;
            imageCell.appendChild(image);
            let notation = document.createElement('div');
            notation.classList.add('work-table-item-notation');
            notation.innerHTML=`<div><div class="work-table-item-images"><img class="pic pic1" src="img/workicon2.png"><img class="pic pic2" src="img/workicon1.png"> </div><div class="work-table-item-text"><h6 class="work-table-item-h6">creative design</h6><p class="work-table-item-p">${item.description}</p></div></div>`;

            imageCell.appendChild(notation);
            imageCell.addEventListener('mouseover',()=>{
                notation.classList.add('visibility');
                notation.classList.remove('work-table-item-notation');

            });
            imageCell.addEventListener('mouseout',()=>{
                notation.classList.remove('visibility');
                notation.classList.add('work-table-item-notation');

            })
        }
    })
}
function webDesign(images) {
    workItem.innerHTML='';
    images.forEach(item => {
        if (item.description === "web design") {
            let imageCell = document.createElement('div');
            imageCell.classList.add("image-cell");
            workItem.appendChild(imageCell);
            let image = document.createElement('img');
            image.src = item.src;
            imageCell.appendChild(image);
            let notation = document.createElement('div');
            notation.classList.add('work-table-item-notation');
            notation.innerHTML=`<div><div class="work-table-item-images"><img class="pic pic1" src="img/workicon2.png"><img class="pic pic2" src="img/workicon1.png"> </div><div class="work-table-item-text"><h6 class="work-table-item-h6">creative design</h6><p class="work-table-item-p">${item.description}</p></div></div>`;

            imageCell.appendChild(notation);
            imageCell.addEventListener('mouseover',()=>{
                notation.classList.add('visibility');
                notation.classList.remove('work-table-item-notation');

            });
            imageCell.addEventListener('mouseout',()=>{
                notation.classList.remove('visibility');
                notation.classList.add('work-table-item-notation');

            })
        }
    })
}
function landingPage(images) {
    workItem.innerHTML='';
    images.forEach(item => {
        if (item.description === "landing page") {
            let imageCell = document.createElement('div');
            imageCell.classList.add("image-cell");
            workItem.appendChild(imageCell);
            let image = document.createElement('img');
            image.src = item.src;
            imageCell.appendChild(image);
            let notation = document.createElement('div');
            notation.classList.add('work-table-item-notation');
            notation.innerHTML=`<div><div class="work-table-item-images"><img class="pic pic1" src="img/workicon2.png"><img class="pic pic2" src="img/workicon1.png"> </div><div class="work-table-item-text"><h6 class="work-table-item-h6">creative design</h6><p class="work-table-item-p">${item.description}</p></div></div>`;

            imageCell.appendChild(notation);
            imageCell.addEventListener('mouseover',()=>{
                notation.classList.add('visibility');
                notation.classList.remove('work-table-item-notation');

            });
            imageCell.addEventListener('mouseout',()=>{
                notation.classList.remove('visibility');
                notation.classList.add('work-table-item-notation');

            })
        }
    })
}
function wordpress(images) {
    workItem.innerHTML='';
    images.forEach((item,) => {
        if (item.description === "wordpress") {
            let imageCell = document.createElement('div');
            imageCell.classList.add("image-cell");
            workItem.appendChild(imageCell);
            let image = document.createElement('img');
            image.src = item.src;
            imageCell.appendChild(image);
            let notation = document.createElement('div');
            notation.classList.add('work-table-item-notation');
            notation.innerHTML=`<div><div class="work-table-item-images"><img class="pic pic1" src="img/workicon2.png"><img class="pic pic2" src="img/workicon1.png"> </div><div class="work-table-item-text"><h6 class="work-table-item-h6">creative design</h6><p class="work-table-item-p">${item.description}</p></div></div>`;

            imageCell.appendChild(notation);
            imageCell.addEventListener('mouseover',()=>{
                notation.classList.add('visibility');
                notation.classList.remove('work-table-item-notation');

            });
            imageCell.addEventListener('mouseout',()=>{
                notation.classList.remove('visibility');
                notation.classList.add('work-table-item-notation');

            })
        }
    })
}

all(images);
let filters = [all, graphicDesign, webDesign, landingPage, wordpress];
for (let i = 0; i<workNavbarItem.length; i++){
    workNavbarItem[i].addEventListener('click',()=>{
        filters[i](images);
    })
}